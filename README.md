# Bouncing Ball

Este é um programa simples de um pequeno programa para executar uma simulação dinâmica (uma bola caindo e saltando por gravidade).

Vamos conhecer a investigar em pormenor a forma como este programa funciona e saber mais sobre as classes de objeto, sua utilização na construção de um programa, e compilação e depuração deste último.

## Requisitos

Esse projeto foi compilador no Ubuntu 16.04.2 LTS (Xenial Xerus).

### Compilador
A versão do compilador usado foi: 

>gcc (Ubuntu 5.4.0-6ubuntu1~16.04.4) 5.4.0 2016 06 09
>Copyright (C) 2015 Free Software Foundation, Inc.

### Gráficos com Octave

Para mostrar gráfico a partir da saída de texto foi usado o programa Octave.
Para instalar octave execute: 

```
$ sudo apt install octave
```

### Gráficos com OpenGL

Para instrucao para instalar pacotes de OpenGL necessarios em Ubuntu:

```
$ sudo apt-get install freeglut3 freeglut3-dev
```

## Descrição

O programa da bola saltitante é implementada no projeto denominado ball. 

Deste projeto foram utilizados inicialmente quatro arquivos: 

>simulation.h, ball.h, ball.cpp e test-ball.cpp. 

Para mostar a bola usando o OpenGL

> graphics.cpp, test-ball-graphics.cpp

### Descrição dos arquivos:

* **simulation.h**: contém uma classe abstrata, com dois métodos virtuais: *step* e *display*.

* **ball.h**: contém a classe *Ball* que herda de simulation, declara variáveis para delimitar a bola e uma caixa na qual ela está inserida.

* **ball.cpp**: implementa os métodos herdados, os contrutores, metódos *set* e *get*.

* **test-ball.cpp**: instancia um objeto da classe *Ball* e chama a função run para fazer o deslocamento da bola.

* **graphics.h**:  arquivo de cabeçalho que inclui elementos da OpenGL.

* **graphics.cpp**: implementa os métodos para criar a janela e a bola.

* **test-ball-graphics.cpp**: abre uma janela e mostra a bola.

## Diagrama de classes 

![diagrama_de_classes](diagrama_de_classes.png)

## Executando

### Usando Octave

Para plotar usando octave execute:

```
$ octave plot_ball.m arquivos/ball.txt
```

### Usando OpenGL

Para compilar e executar:

```
g++ -lGL -lGLU -lglut ball.cpp graphics.cpp test-ball-graphics.cpp - o ball && ./ball
```

### Saída em texto:

Ver completo em [ball.txt](https://gitlab.com/welton.braga/tecnicas-de-programacao-1/blob/master/arquivos/ball.txt).


```
-0.1 0.0278889
-0.2 0.0448889
-0.3 0.051
-0.4 0.0462222
-0.5 0.0305556
-0.6 0.004
-0.7 -0.0334444
-0.8 -0.0817778
-0.9 -0.141
-0.9 -0.211111
-0.8 -0.292111
-0.7 -0.384
-0.6 -0.486778
-0.5 -0.600444
-0.4 -0.725
-0.3 -0.860444
-0.2 -0.860444
-0.1 -0.725
-2.77556e-17 -0.600444
0.1 -0.486778
0.2 -0.384
0.3 -0.292111
0.4 -0.211111
0.5 -0.141
0.6 -0.0817778
0.7 -0.0334444
0.8 0.004
0.9 0.0305556
0.9 0.0462222
0.8 0.051
0.7 0.0448889
0.6 0.0278889
0.5 4.68375e-17
0.4 -0.0387778
0.3 -0.0884444
0.2 -0.149
0.1 -0.220444
2.77556e-17 -0.302778
-0.1 -0.396
-0.2 -0.500111
-0.3 -0.615111
-0.4 -0.741
-0.5 -0.877778
-0.6 -0.877778
-0.7 -0.741
-0.8 -0.615111
-0.9 -0.500111
-0.9 -0.396
-0.8 -0.302778
-0.7 -0.220444
-0.6 -0.149
-0.5 -0.0884444
-0.4 -0.0387778
-0.3 1.73472e-18
-0.2 0.0278889
-0.1 0.0448889
-2.77556e-17 0.051
0.1 0.0462222
0.2 0.0305556
0.3 0.004
0.4 -0.0334444
0.5 -0.0817778
0.6 -0.141
0.7 -0.211111
0.8 -0.292111
0.9 -0.384
0.9 -0.486778
0.8 -0.600444
0.7 -0.725
0.6 -0.860444
0.5 -0.860444
0.4 -0.725
0.3 -0.600444
0.2 -0.486778
0.1 -0.384
2.77556e-17 -0.292111
-0.1 -0.211111
-0.2 -0.141
-0.3 -0.0817778
-0.4 -0.0334444
-0.5 0.004
-0.6 0.0305556
-0.7 0.0462222
-0.8 0.051
-0.9 0.0448889
-0.9 0.0278889
-0.8 4.68375e-17
-0.7 -0.0387778
-0.6 -0.0884444
-0.5 -0.149
-0.4 -0.220444
-0.3 -0.302778
-0.2 -0.396
-0.1 -0.500111
-2.77556e-17 -0.615111
0.1 -0.741
0.2 -0.877778
0.3 -0.877778
0.4 -0.741
0.5 -0.615111
```

### Primeiro gráfico gerado no excel:

![grafico_excel](grafico_bola_excel.png)

# SpringMass

 Implementada classe SpringMass.

## Descrição

Simula movimento de massas presas por molas.

## Compilador

>gcc (Ubuntu 5.4.0-6ubuntu1~16.04.4) 5.4.0 2016 06 09
>Copyright (C) 2015 Free Software Foundation, Inc.

Linha de comando para compilação:


```
$ g++ springmass.cpp test-springmass.cpp -o "nome do executavel"
```

## Visualização do gráfico usando Octave


Execute os comandos no terminal:

```
$ ./springmass > arquivos/springmass.txt
$ octave plot_springMass.m arquivos/springmass.txt
```
    
# Gráficos

O arquivo **graphics.h** declara duas classes:

* **classe Drawable** : possui apenas um método virtual draw.

* **classe Figure** : herda da classe *Drawable* e declara varios outros métodos, entre eles os que são usados para desenhar linha, circulo e string.

O arquivo **graphics.cpp**: 

* implementa os métodos das classes descritas acima. Utiliza recursos da openGL e Glut.

* implementa funções globais, como a função *run*, que cria o loop para mostrar as imagens na janela.

## Diagrama de classes ball graphics

![diagrama graphics ball](diagrama-graphics-ball.png)

## Diagrama de Sequência v0.1

![diagrama de sequencia ](diagrama_de_sequencia_graphics.png)

Código usado para criar o driagrama:
https://www.websequencediagrams.com/

```
title Diagrama de Sequência: graphics
SpringMassDrawable->Figure: SpringMassDrawable()
Figure->Figure: Figure()
Figure->Graphics: addDrawable()
SpringMassDrawable->SpringMassDrawable: addMass()
SpringMassDrawable->SpringMassDrawable: addSpring()
SpringMassDrawable->Graphics: run()
Figure->Graphics: draw()
Figure->Graphics: display()
```

## Diagrama de Sequência v.02 (mais detalhado)

![Diagrama mais detalhado](diagrama_de_sequencia_graphics_v2.png)

```
title Diagrama de Sequência: graphics
SpringMassDrawable->Figure: SpringMassDrawable()
SpringMassDrawable->Figure: Figure()
Figure->Graphics: addDrawable()
SpringMassDrawable->Mass: Mass()
SpringMassDrawable->SpringMassDrawable: addMass()
SpringMassDrawable->Spring: Spring()
SpringMassDrawable->SpringMassDrawable: addSpring()
SpringMassDrawable->Graphics: run()
loop glutMainLoop
    Graphics->SpringMassDrawable: handleTimer()
    SpringMassDrawable->Mass: step()
    Mass->Mass:setForce()
    Spring->Spring:getForce()
    Spring->Mass:addForce()
    Mass->SpringMassDrawable:step()
    SpringMassDrawable->Figure: draw()
    Figure->Graphics: drawCircle()
    Figure->Graphics: drawLine()
    Figure->Graphics: drawString()
    SpringMassDrawable->Figure: display()
    Figure->Graphics: update()
end
```

## Captura de Tela Sprigmass funcionando

![captura de tela springmass](captura_de_tela_springmass.png)