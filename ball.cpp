/** file: ball.cpp
 ** brief: Ball class - implementation
 ** author: Andrea Vedaldi
 **/

#include "ball.h"

#include <iostream>
#include <iomanip>
#include <utility> 


Ball::Ball()
: r(0.1), x(0), y(0), vx(0.3), vy(-0.1), g(9.8), m(1),
xmin(-1), xmax(1), ymin(-1), ymax(1)
{ }

Ball::Ball(double valor_x, double valor_y, double vel_x, double vel_y)
: r(0.1), x(valor_x), y(valor_x), vx(vel_x), vy(vel_y), g(9.8), m(1),
xmin(-1), xmax(1), ymin(-1), ymax(1)
{ }


void Ball::setPosX_Y(double pos_x, double pos_y) 
{
  x = pos_x;
  y = pos_y;
}

void Ball::setVx_Vy(double vel_x, double vel_y){
  vx = vel_x;
  vy = vel_y;
}

// retorna o par x e y
std::pair<double, double> Ball::getCoordenadasXeY(){
  return std::make_pair (x,y);
}

void Ball::step(double dt)
{
  double xp = x + vx * dt ;
  double yp = y + vy * dt - 0.5 * g * dt * dt ;
  if (xmin + r <= xp && xp <= xmax - r) {
    x = xp ;
  } else {
    vx = -vx ;
  }
  if (ymin + r <= yp && yp <= ymax - r) {
    y = yp ;
    vy = vy - g * dt ;
  } else {
    vy = -vy ;
  }
}
  
void Ball::display()
{
  std::cout<<x<<" "<<y<<std::endl ;
  //std::cout << std::fixed << std::setprecision(3) << x <<" "<< y << std::endl ;
}
