/** file: ball.h
 ** brief: Ball class
 ** author: Andrea Vedaldi
 ** Modified by: Welton Braga
 **/

#ifndef __ball__
#define __ball__

#include "simulation.h"
#include <utility> 

class Ball : public Simulation
{
public:
  /// Construtor padrão
  Ball() ;
  /// Construtor com opções de coordenadas x e y e velocidade x e y
  Ball(double valor_x, double valor_y, double vel_x, double vel_y);
 
  /// métodos tipo setter
  void setPosX_Y(double pos_x, double pos_y);///< definde valor das coordenadas x e y 
  void setVx_Vy(double vel_x, double vel_y);///< retorna um par com as coordenadas x e y 

  std::pair<double, double> getCoordenadasXeY();
  /// Atualiza velocidade e posição da Ball
  void step(double dt) ;
  /// mostra as coordenadas x e y da Ball
  void display() ;

protected:


  /** @defgroup Dados_da_bola
  *  Armazena coordenadas da posição e velocidade da bola. Além da massa e o raio.
  *  @{
  */
  /** @brief posição x*/
  double x ;
  /** @brief posição y*/
  double y ;
  /** @brief velocidade x*/
  double vx ;
  /** @brief velocidade y*/
  double vy ;
  /// massa
  double m ;
  /// raio
  double r ;
  /** @} */ // end Dados_da_bola
  /// aceleração da gravidade
  double g ; 

  /** @defgroup Limites_da_caixa
  *  Define as coordenadas máximas de alcance para x e y. 
  *  Simula uma caixa que vai de -1 a +1 no plano cartesiano.
  *  @{
  */

  /** Valores de -1 a +1 */
  double xmin ;
  /** Valores de -1 a +1 */
  double xmax ;
  /** Valores de -1 a +1 */
  double ymin ;
  /** Valores de -1 a +1 */
  double ymax ;

  /** @} */ // end Limites_da_caixa
} ;

#endif /* defined(__ball__) */
