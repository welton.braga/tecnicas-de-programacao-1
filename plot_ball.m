arg_list = argv ();
num_args = length (arg_list);
if (num_args == 1)
	ball=load(arg_list{1});
	figure;
	%hold on; 
	axis([-1, 1, -1, 1])
	for c=1:size(ball,1),
	 	 plot(ball(c,1), ball(c,2), 'r*', 'MarkerSize', 40, 'LineWidth', 5);
	   axis([-1, 1, -1, 1])
	   pause(.01);
	end
else
	 printf('[Ops] Faltou o arquivo de dados, exemplo: $ octave %s Ball.txt',program_name());
end
