arg_list = argv ();
num_args = length (arg_list);
if (num_args == 1)
	sm=load(arg_list{1});
	figure;
	%hold on; 
	axis([-1, 1, -1, 1])
	for c=1:size(sm,1),
	 	plot(sm(c,1), sm(c,2), 'k*', 'MarkerSize', 20, 'LineWidth', 5, 
	 		sm(c,3), sm(c,4), 'ro', 'MarkerSize', 10, 'LineWidth', 5,
	 		[sm(c,1); sm(c,3)], [sm(c,2); sm(c,4)], '-');
	   	axis([-1, 1, -1, 1]);
	    pause(.01);
	end
else
	printf('\nOps! Faltou o arquivo de dados, exemplo: $ octave %s ''ARQUIVO.txt'' \n',program_name());
end