/** file: test-ball.cpp
 ** brief: Tests the bouncing ball simulation
 ** author: Andrea Vedaldi
 **/

#include "ball.h"

void run (Simulation &s, double dt){
	for (int i = 0; i < 1000; ++i)
	{
		s.step(dt);
		s.display();
	}
}

int main(int argc, char** argv)
{
  Ball ball(0,0,-3,1);
  const double dt = 1.0/30 ;
  // substituido o for pela função run
  run(ball, dt);
  return 0 ;
}
