/** file: test-springmass-graphics.cpp
 ** brief: Tests the spring mass simulation with graphics
 ** author: Andrea Vedaldi
 **/

#include "graphics.h"
#include "springmass.h"

#include <iostream>
#include <sstream>
#include <iomanip>

/* ---------------------------------------------------------------- */
class SpringMassDrawable : public SpringMass, public Drawable
/* ---------------------------------------------------------------- */
{
private:
  Figure figure ;

public:
  SpringMassDrawable()
  : figure("Spring Mass openGL")
  {
    figure.addDrawable(this) ;
  } 

  void draw() {
    for (int i = 0; i < vet_mass.size(); ++i)
    {
      figure.drawCircle(vet_mass[i]->getPosition().x, vet_mass[i]->getPosition().y, vet_mass[i]->getRadius()) ;
    }
    for (int i = 0; i < vet_spring.size(); ++i)
    {
      figure.drawLine(vet_spring[i]->getMass1()->getPosition().x,vet_spring[i]->getMass1()->getPosition().y,
        vet_spring[i]->getMass2()->getPosition().x, vet_spring[i]->getMass2()->getPosition().y, 0.5);
    }
    figure.drawString(-.8, +.8, "Trab TP1 - Welton");
  }

  void display() {
    figure.update() ;
  } 
} ;

int main(int argc, char** argv)
{
  glutInit(&argc,argv) ;

  SpringMassDrawable springmass;
  double mass = 0.05 ;
  double radius =  0.02 ;
  double naturalLength = 0.95 ;
  double stiffness = 1.0 ;
  
  Mass m1(Vector2(-.5,+.7), Vector2(), mass, radius) ;
  Mass m2(Vector2(0.0,+.8), Vector2(), mass, radius) ;
  Mass m3(Vector2(+.5,+.9), Vector2(), mass, radius) ;
  Spring s1(&m1, &m2, naturalLength, stiffness);
  Spring s2(&m2, &m3, naturalLength, stiffness);

  springmass.addMass(&m1);
  springmass.addMass(&m2);
  springmass.addMass(&m3);
  springmass.addSpring(&s1);
  springmass.addSpring(&s2);
  
/* falha de segmentação
using namespace std;
  std::vector<Mass> m;
  double x = -.9;
  double y = +.9;
  for (int i = 0; i < 3; ++i)
  {
    m.push_back(Mass(Vector2(x, y), Vector2(), mass, radius));
    x += .2;
    y -= .1;
    springmass.addMass(&m.at(i));
  }
  std::vector<Spring> s;
  for (int i = 0; i < 2; ++i)
  {
    s.push_back(Spring(&m[i], &m[i+1], naturalLength, stiffness));
    cout << i << endl;
    springmass.addSpring(&s.at(i));
  }
  */
   run(&springmass, 1/120.0) ;
}
