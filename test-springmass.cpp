/** file: test-srpingmass.cpp
 ** brief: Tests the spring mass simulation
 ** author: Andrea Vedaldi
 **/

#include "springmass.h"
#include <iostream>
using namespace std;

int main(int argc, char** argv)
{
  SpringMass springmass ;

  const double mass = 0.05 ;
  const double radius = 0.02 ;
  const double naturalLength = 0.95 ;
  const double stiffness = 1.0 ;


  //Mass m1(Vector2(-.5,0.8), Vector2(0,-0.5), mass, radius) ;
  Mass m1(Vector2(-0.5,0.9), Vector2(), mass, radius) ;
  Mass m2(Vector2(+0.5,0.8), Vector2(), mass, radius) ;
  Spring mola(&m1, &m2, naturalLength, stiffness);
/* INCOMPLETE: TYPE YOUR CODE HERE */
  springmass.addMass(&m1);
  springmass.addMass(&m2);
  springmass.addSpring(&mola);

  const double dt = 1.0/30 ;
  for (int i = 0 ; i < 1000 ; ++i) {
    springmass.step(dt) ;
    springmass.display() ;
    //cout << "e: " << springmass.getEnergy() << endl;
  }
 return 0;
}
